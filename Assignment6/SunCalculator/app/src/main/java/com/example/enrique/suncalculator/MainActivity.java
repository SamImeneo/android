package com.example.enrique.suncalculator;

import android.content.Context;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    ArrayList<Location> World;
    ArrayList<String> SpinnerData;

    Spinner spinnerList;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        World = new ArrayList<Location>();

        SpinnerData = new ArrayList<String>();
        ArrayList<String> SpinnerDataTZ = new ArrayList<String>();

        SpinnerDataTZ.add("Australia/ACT");
        SpinnerDataTZ.add("Australia/ACT");
        SpinnerDataTZ.add("Australia/Adelaide");
        SpinnerDataTZ.add("Australia/Brisbane");
        SpinnerDataTZ.add("Australia/Broken_Hill");
        SpinnerDataTZ.add("Australia/Canberra");
        SpinnerDataTZ.add("Australia/Currie");
        SpinnerDataTZ.add("Australia/Darwin");
        SpinnerDataTZ.add("Australia/Eucla");
        SpinnerDataTZ.add("Australia/Hobart");
        SpinnerDataTZ.add("Australia/LHI");
        SpinnerDataTZ.add("Australia/Lindeman");
        SpinnerDataTZ.add("Australia/Lord_Howe");
        SpinnerDataTZ.add("Australia/Melbourne");
        SpinnerDataTZ.add("Australia/NSW");
        SpinnerDataTZ.add("Australia/North");
        SpinnerDataTZ.add("Australia/Perth");
        SpinnerDataTZ.add("Australia/Queensland");
        SpinnerDataTZ.add("Australia/South");
        SpinnerDataTZ.add("Australia/Sydney");
        SpinnerDataTZ.add("Australia/Tasmania");
        SpinnerDataTZ.add("Australia/Victoria");
        SpinnerDataTZ.add("Australia/West");
        SpinnerDataTZ.add("Australia/Yancowinna");

        spinnerList = (Spinner) findViewById(R.id.spinlocation);
        spinner = (Spinner)findViewById(R.id.timeZoneSpinner);

        ArrayAdapter<String> dataAdapterTZ = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item ,SpinnerDataTZ);
        dataAdapterTZ.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(dataAdapterTZ);
        spinner.setOnItemSelectedListener(this);


        String filename = "locations.txt";

        ReadFileCreateData();
        try {
            FileInputStream fIn = openFileInput(filename);
            ReadFileCreateData(fIn);
            Log.i("TET", "end of try in con");
            fIn.close();
        }catch (IOException e) {
            e.printStackTrace();
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item ,SpinnerData);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerList.setAdapter(dataAdapter);
        spinnerList.setOnItemSelectedListener(this);

        initializeUI();
    }




    private void initializeUI()
    {


        DatePicker datePicker = (DatePicker) findViewById(R.id.datePicker);
        Calendar cal = Calendar.getInstance();
        // get current date
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        // set date picker to that date
        datePicker.init(year,month,day,dateChangeHandler);

        updateTime(year, month, day);
    }

    private void updateTime(int year, int month, int day)
    {

        String loc;
        // set the location to the selected item
        loc = spinnerList.getSelectedItem().toString();
        int selectedLoc = 0;

        // for each of the locations in the world array, check if it has the same name as the selected location
        for (int i = 0; i < World.size(); i++)
        {
            if(World.get(i).getName().equals(loc))
            {
                selectedLoc = i;
                break;
            }
        }

        // set the new location
        GeoLocation geoLoc = new GeoLocation(World.get(selectedLoc).getName(), World.get(selectedLoc).get_latitude(), World.get(selectedLoc).get_longitude(), World.get(selectedLoc).get_timezone());
        AstronomicalCalendar AC = new AstronomicalCalendar(geoLoc);

        AC.getCalendar().set(year, month, day);
        String str = World.get(selectedLoc).get_timezone().toString();

        // split the string by /
        String[] split = str.split("/");

        // 01:24, is in 24 hour time
        SimpleDateFormat _simple = new SimpleDateFormat("HH:mm");

        TextView sunriseTV = (TextView) findViewById(R.id.sunriseTimeTV);
        TextView sunsetTV = (TextView) findViewById(R.id.sunsetTimeTV);

        sunriseTV.setText(_simple.format(AC.getSunrise()));
        sunsetTV.setText(_simple.format(AC.getSunset()));
    }

    DatePicker.OnDateChangedListener dateChangeHandler = new DatePicker.OnDateChangedListener()
    {
        public void onDateChanged(DatePicker dp, int year, int month, int day)
        {
            updateTime(year, month, day);
        }
    };

    void ReadFileCreateData() {
        String stringFromFile = "";
        String[] split;
        // the text file to read from



        //Environment.getExternalStorageDirectory().getAbsolutePath().toString()+"/com.example.enrique/locations.txt"

        InputStream is = this.getResources().openRawResource(R.raw.locations);
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));

            if(!SpinnerData.isEmpty())
            {
                SpinnerData.clear();
            }
            if(!World.isEmpty())
            {
                World.clear();
            }

            // if the text file isnt null
            if (is != null) {

                //while each line isnt null
                while ((stringFromFile = reader.readLine()) != null) {
                    // read the line, split by ,
                    split = stringFromFile.split(",");
                    // add to array
                    SpinnerData.add(split[0]);
                    Log.i("TEST", "adding to spinner");
                    Log.i("TEST", "Name: " + split[0]);
                    Log.i("TEST", "Lat: " + split[1]);
                    Log.i("TEST", "Long: " + split[2]);
                    Log.i("TEST", "TZone: " + split[3]);
                    // add the location to the array
                    World.add(new Location(split[0], Float.parseFloat(split[1]), Float.parseFloat(split[2]), TimeZone.getTimeZone(split[3])));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


        }
    }

    void ReadFileCreateData(FileInputStream fIn) {
        String stringFromFile = "";
        String[] split;


        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fIn));

            // if the text file isnt null
            if (fIn != null) {

                //while each line isnt null
                while ((stringFromFile = reader.readLine()) != null) {
                    // read the line, split by ,
                    split = stringFromFile.split(",");
                    // add to array
                    SpinnerData.add(split[0]);
                    Log.i("TEST", "adding to spinner");
                    Log.i("TEST", "Name: " + split[0]);
                    Log.i("TEST", "Lat: " + split[1]);
                    Log.i("TEST", "Long: " + split[2]);
                    Log.i("TEST", "TZone: " + split[3]);
                    // add the location to the array
                    World.add(new Location(split[0], Float.parseFloat(split[1]), Float.parseFloat(split[2]), TimeZone.getTimeZone(split[3])));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fIn != null) {
                try {
                    fIn.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


        }
    }



    // when an item is selected
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        initializeUI();
    }

    // currently not user
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
    public void addLocation(View v)
    {

        EditText locName = (EditText)findViewById(R.id.locationNameEditText);
        EditText locLat = (EditText)findViewById(R.id.latitudeEditText);
        EditText locLong = (EditText)findViewById(R.id.longitudeEditText);







        spinner = (Spinner) findViewById(R.id.timeZoneSpinner);

        String locationName = locName.getText().toString();
        String locationLatitude = locLat.getText().toString();
        String locationLongitude = locLong.getText().toString();
        String locationTimeZone = spinner.getSelectedItem().toString();



        if(locationName.matches("")) {
            Toast.makeText(this, "Invalid Location Name", Toast.LENGTH_SHORT).show();
            return;
        }
        else if(locationLatitude.matches(""))
        {
            Toast.makeText(this, "Invalid Latitude", Toast.LENGTH_SHORT).show();
            return;
        }
        else if(Double.parseDouble(locationLatitude) < -90 || Double.parseDouble(locationLatitude) > 90) {
            Toast.makeText(this, "Latitude must be between -90 and 90", Toast.LENGTH_SHORT).show();
            return;
        }
        else if(locationLongitude.matches(""))
        {
            Toast.makeText(this, "Invalid Longitude", Toast.LENGTH_SHORT).show();
            return;
        }
        else if(Double.parseDouble(locationLongitude) < -180 || Double.parseDouble(locationLongitude) > 180) {
            Toast.makeText(this, "Longitude must be between -180 and 180", Toast.LENGTH_SHORT).show();
            return;
        }
        else if(locationTimeZone.matches(""))
        {
            Toast.makeText(this, "Invalid TimeZone", Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            // TimeZone.getTimeZone(LocationTimeZone);
            try {
                Log.i("TEST", "start of try");

                //FileOutputStream fOut = new FileOutputStream(new File("raw/locations"));

                String fileName = "locations.txt";


                //File asd = new File(Environment.getExternalStorageDirectory().getAbsolutePath().toString()+"/com.example.enrique/locations.txt");

                Log.i("TEST", "file works");
                FileOutputStream fOut = openFileOutput(fileName, MODE_APPEND);

                //FileOutputStream fOut = new FileOutputStream(new File(fileName),true);


                Log.i("TEST", "stream works");

                OutputStreamWriter out = new OutputStreamWriter(fOut);

                Log.i("TEST", locationName + "," + locationLatitude + "," + locationLongitude + "," + locationTimeZone);

                out.write(locationName);
                out.write(",");
                out.write(locationLatitude);
                out.write(",");
                out.write(locationLongitude);
                out.write(",");
                out.write(locationTimeZone);
                out.write('\n');
                out.close();
                fOut.close();

                FileInputStream fIn = openFileInput(fileName);

                ReadFileCreateData(fIn);

                fIn.close();



                initializeUI();

                Log.i("TEST", "end of try");

                for (int i = 0; i < TimeZone.getAvailableIDs().length; i++) {
                    if(!TimeZone.getAvailableIDs()[i].toString().equals(""))
                    {
                        Log.i("TZ ID", TimeZone.getAvailableIDs()[i].toString());
                    }
                }

                locName.setText("");
                locLat.setText("");
                locLong.setText("");

                Toast.makeText(this, "Location Added", Toast.LENGTH_SHORT).show();

                //TimeZone.getAvailableIDs();

            }
            catch (java.io.IOException e)
            {
                Toast.makeText(this, "Could not add Location", Toast.LENGTH_SHORT).show();
                Log.i("TEST", "catch here");
            }

        }
    }


}
