package com.example.enrique.suncalculator;

import java.util.TimeZone;

/**
 * Created by 7681518 on 10/19/16.
 */
public class Location {

    private String _name;
    private double _latitude;
    private double _longitude;
    private TimeZone _timezone;

    public Location(String Name, float Lat, float Long, TimeZone timywimey)
    {
          _name = Name;
         _latitude = Lat;
         _longitude = Long;
         _timezone = timywimey;
    }
    public  String getName(){
        return _name;
    }

    public double get_latitude(){
        return _latitude;
    }

    public double get_longitude(){
        return _longitude;
    }

    public TimeZone get_timezone(){
        return _timezone;
    }

}
