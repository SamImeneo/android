package com.example.enrique.assignment6task2;

public class Book {
    private int rating;

    public Book(int tempRating)
    {
        this.rating = tempRating;
    }

    public String toString()
    {
        return rating + "*" + "/10" + "*";
    }
}
