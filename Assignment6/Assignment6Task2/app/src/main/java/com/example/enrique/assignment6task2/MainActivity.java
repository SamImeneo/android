package com.example.enrique.assignment6task2;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.HashMap;

public class MainActivity extends ListActivity {

    private HashMap<String, Book> books;
    private String[] bookTitles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        books = loadBookData();
        bookTitles = getBookTitles();
        initializeUI();
    }

    private void initializeUI()
    {
        // use custom row adapter
        setListAdapter(new RowIconAdapter());
        // force to display information about the Hobbit
        displaySelectedBook("The Hobbit");
    }

    private String[] getBookTitles()
    {
        String[] bookArray = new String[books.size()];
        bookArray = books.keySet().toArray(bookArray);
        return bookArray;
    }

    // when a book in the list is clicked
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        // get the position of the click, get the book at that position
        String selectedItem = (String) getListView().getItemAtPosition(position);
        //display that book
        displaySelectedBook(selectedItem);
    }


    private void displaySelectedBook(String bookName)
    {
        // get the book from the list of books
        Book book = books.get(bookName);

        //set the text views
        TextView bookTitleTextView = (TextView) findViewById(R.id.bookTitleTextView);
        TextView bookRatingTextView = (TextView) findViewById(R.id.bookRatingTextView);
        ImageView bookImageView = (ImageView) findViewById(R.id.bookImage);


        bookTitleTextView.setText(bookName);
        if (book != null) {
            bookRatingTextView.setText(book.toString());

            if (bookName.startsWith("The Hob"))
                bookImageView.setImageResource(R.drawable.hobbit);
            else if (bookName.startsWith("LOTR: fel"))
                bookImageView.setImageResource(R.drawable.fellowship);
            else if (bookName.startsWith("LOTR: two"))
                bookImageView.setImageResource(R.drawable.twotowers);
            else if (bookName.startsWith("LOTR: return"))
                bookImageView.setImageResource(R.drawable.returnofking);
            else if (bookName.startsWith("Harry potter: sto"))
                bookImageView.setImageResource(R.drawable.stone);
            else if (bookName.startsWith("Harry potter: flying"))
                bookImageView.setImageResource(R.drawable.car);
            else if (bookName.startsWith("Harry potter: prison"))
                bookImageView.setImageResource(R.drawable.prison);
            else if (bookName.startsWith("Harry potter: tou"))
                bookImageView.setImageResource(R.drawable.cup);
            else if (bookName.startsWith("Harry potter: pho"))
                bookImageView.setImageResource(R.drawable.phoenix);
            else if (bookName.startsWith("Harry potter: half"))
                bookImageView.setImageResource(R.drawable.halfblood);
            else
                bookImageView.setImageResource(R.drawable.deficon);
        }




    }

    private HashMap<String, Book> loadBookData()
    {
        HashMap<String, Book> books = new HashMap<String, Book>();
        books.put("The Hobbit", new Book(7));
        books.put("LOTR: fellowship", new Book(8));
        books.put("LOTR: two towers", new Book(4));
        books.put("LOTR: return of king", new Book(6));
        books.put("Harry potter: stone", new Book(3));
        books.put("Harry potter: flying car", new Book(7));
        books.put("Harry potter: prison", new Book(10));
        books.put("Harry potter: tournament", new Book(9));
        books.put("Harry potter: phoenix", new Book(4));
        books.put("Harry potter: half blood prince", new Book(6));
        return books;
    }

    class RowIconAdapter extends ArrayAdapter<String>
    {
        public RowIconAdapter()
        {
            super(MainActivity.this, R.layout.listrow, R.id.rowBookTitle, bookTitles);
        }

        public View getView(int pos, View cView,  ViewGroup parent)
        {
            View row = super.getView(pos, cView, parent);

            // get current image
            ImageView image = (ImageView) row.findViewById(R.id.rowBookImage);

            String book = bookTitles[pos];


            //find the image base don the name
            if (book.startsWith("The Hob"))
                image.setImageResource(R.drawable.hobbit);
            else if (book.startsWith("LOTR: fel"))
                image.setImageResource(R.drawable.fellowship);
            else if (book.startsWith("LOTR: two"))
                image.setImageResource(R.drawable.twotowers);
            else if (book.startsWith("LOTR: return"))
                image.setImageResource(R.drawable.returnofking);
            else if (book.startsWith("Harry potter: sto"))
                image.setImageResource(R.drawable.stone);
            else if (book.startsWith("Harry potter: flying"))
                image.setImageResource(R.drawable.car);
            else if (book.startsWith("Harry potter: prison"))
                image.setImageResource(R.drawable.prison);
            else if (book.startsWith("Harry potter: tou"))
                image.setImageResource(R.drawable.cup);
            else if (book.startsWith("Harry potter: pho"))
                image.setImageResource(R.drawable.phoenix);
            else if (book.startsWith("Harry potter: half"))
                image.setImageResource(R.drawable.halfblood);
            else
                image.setImageResource(R.drawable.deficon);

            return row;
        }
    }



}
